function getTime() {
	var hrTime = process.hrtime()
	return hrTime[0] * 1000000 + hrTime[1] / 1000;
}

function addRequest(data, type) {
	if (!requests[data.id]) {
		requests[data.id] = [];
	}
	data.time = getTime();
	data.type = type;
	requests[data.id].push(data);
	return requests[data.id];
}

var requests = {};
var clear = {};

var dgram = require('dgram');
var udpServer = dgram.createSocket('udp4');
udpServer.on('message', function(message, remote) {
	console.log(remote.address + ':' + remote.port + ' udp (' + getTime() + ') - ' + message);
	addRequest(JSON.parse(message), 'udp');
});
udpServer.bind(11111);

// echo -n '{"id":"ab12"}' >/dev/udp/127.0.0.1/11111

var net = require('net');
var tcpServer = net.createServer(function(socket) {
	socket.setEncoding('utf8');
	socket.on('data', function(data) {
		console.log(socket.remoteAddress + ':' + socket.remotePort + ' tcp (' + getTime() + ') - ' + data);
		addRequest(JSON.parse(data), 'tcp');
	});
});
tcpServer.listen(22222);

// echo -n '{"id":"ab12"}' >/dev/tcp/127.0.0.1/22222

var express = require('express');
var bodyParser = require('body-parser');
var http = express();
http.use(bodyParser.urlencoded({ extended: true }));
http.use(bodyParser.json());
var router = express.Router();
router.post('/execute', function(req, res) {
	console.log(req.client.remoteAddress + ':' + req.client.remotePort + ' http (' + getTime() + ') - ' + JSON.stringify(req.body));
	var requests = addRequest(req.body, 'http');
	var sortedRequests = requests.sort(function(a, b) {
		return a.time < b.time ? -1 : (a.time > b.time ? 1 : 0);
	});
	if (!clear[req.body.id]) {
		clear[req.body.id] = sortedRequests[0].time;
	}
	var response = {
		performance: {
			fastest: sortedRequests[0].type,
			fastestMs: (sortedRequests[0].time - clear[req.body.id]) / 1000,
			slowest: sortedRequests[sortedRequests.length-1].type,
			slowestMs: (sortedRequests[sortedRequests.length-1].time - clear[req.body.id]) / 1000,
			differenceMs: (sortedRequests[sortedRequests.length-1].time - sortedRequests[0].time) / 1000
		},
		requests: sortedRequests
	};
	console.log(response);
	res.json(response);
});
router.post('/clear', function(req, res) {
	if (req.body.id) {
		requests[req.body.id] = [];
	}
	clear[req.body.id] = getTime();
	res.json();
});
http.use(router);
http.listen(80);

// curl -X POST -i -H "Content-type: application/json" -X POST http://127.0.0.1:80/ -d '{"id":"ab12"}'

